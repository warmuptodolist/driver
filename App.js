/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import firebase from 'react-native-firebase';
import Notification from './src/lib/Notification';
import { NOTIFICATION, client } from './src/config';
import { ApolloProvider } from 'react-apollo';

import Home from './src/screens/Home/index';
export default class App extends Component {
	async componentDidMount() {
		try {
			let enable = await Notification.hasPermission();
			if (!enable) {
				console.log('condition here');
				await Notification.askPermission();
			}
		} catch (e) {}
	}

	render() {
		return (
			<ApolloProvider client={client}>
				<Home />
			</ApolloProvider>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#F5FCFF'
	},
	welcome: {
		fontSize: 20,
		textAlign: 'center',
		margin: 10
	},
	instructions: {
		textAlign: 'center',
		color: '#333333',
		marginBottom: 5
	}
});
