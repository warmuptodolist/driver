import React, { Component } from 'react';
import { Text, View, FlatList, ActivityIndicator } from 'react-native';
import { Query, Mutation } from 'react-apollo';
import DeliveryItem from '../DeliveryItem';
import gql from 'graphql-tag';
import Dialog from 'react-native-dialog';

const GET_CUSTOMER = gql`
	{
		listAllUser {
			id
			name
			token
		}
	}
`;

const DELIVERY_ITEM = gql`
	mutation DeliveryItem($userId: String!, $itemName: String!) {
		deliveryItem(userId: $userId, itemName: $itemName)
	}
`;

export default class DeliveryList extends Component {
	state = {
		isDialogVisible: false,
		user: {},
		itemName: '',
	};
	componentDidMount() {}

	renderItem = ({ item }) => {
		let { name } = item;
		return <DeliveryItem onRightButtonPress={() => this.handleInputDeliveryItem(item)} name={name || ''} />;
	};

	handleInputDeliveryItem = (user) => {
		this.setState({
			isDialogVisible: true,
			user,
		});
	};

	handleConfirmDeliveryItem = async (fn) => {
		let { id } = this.state.user;
		let { itemName } = this.state;
		let payload = {
			userId: id,
			itemName,
		};

		console.log('this is payload for mutation ====>', payload);
		try {
			let response = await fn({ variables: payload });
			console.log('this is response =====>', response);
			this.handleCloseInputDeliveryItem();
		} catch (e) {
			console.log('error while delivering', JSON.stringify(e));
		}
	};

	handleCloseInputDeliveryItem = () => {
		this.setState({
			isDialogVisible: false,
		});
	};

	handleChangeItemName = (itemName) => {
		this.setState({
			itemName,
		});
	};
	renderInputDeliveryItem = () => {
		let { itemName, isDialogVisible } = this.state;

		return (
			<Mutation mutation={DELIVERY_ITEM}>
				{(deliveryItem) => {
					return (
						<Dialog.Container visible={isDialogVisible}>
							<Dialog.Input
								label="Enter Item"
								value={itemName}
								onChangeText={this.handleChangeItemName}
							/>
							<Dialog.Button label="Cancel" onPress={this.handleCloseInputDeliveryItem} />
							<Dialog.Button
								label="Start Delivering"
								onPress={() => this.handleConfirmDeliveryItem(deliveryItem)}
							/>
						</Dialog.Container>
					);
				}}
			</Mutation>
		);
	};

	render() {
		let { isDialogVisible } = this.state;
		return (
			<Query query={GET_CUSTOMER}>
				{({ data, error, loading }) => {
					if (error) {
						console.log(JSON.stringify(error));
						return <Text>Something went wrong</Text>;
					}
					if (loading) {
						return <ActivityIndicator size="large" color="#0000ff" />;
					}
					let { listAllUser } = data;
					return (
						<View style={{ width: '100%' }}>
							<FlatList renderItem={this.renderItem} data={listAllUser || []} style={{ width: '100%' }} />
							{this.renderInputDeliveryItem()}
						</View>
					);
				}}
			</Query>
		);
	}
}
