export const NOTIFICATION = {
	CHANNEL: {
		IMPORTANCE: {
			DEFAULT: 'Default', //shows everywhere, makes noise, but does not visually intrude
			HIGH: 'High', //shows everywhere, makes noise and peeks. May use full screen intents
			LOW: 'Low', //shows everywhere, but is not intrusive
			MAX: 'Max', //Unused.
			MIN: 'Min', // shows in the shade, below the fold.
			NONE: 'None', //does not show in the shade
			UNSPECIFIED: 'Unspecified', //Value signifying that the user has not expressed an importance
		},
	},
};

export const NOTIFICATION_TOPIC = {
	REJECTED_DELIVERY: 'rejected_delivery',
	ACCEPTED_DELIVERY: 'accepted_delivery',
};

export const SERVER_URL = 'http://192.168.2.111:4000/';
