import React, { Component } from 'react';
import { View } from 'react-native';
import { Container, Header, Content, List, ListItem, Text, Icon, Left, Body, Right, Switch, Button } from 'native-base';
export default class DeliveryItem extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	static defaultProps = {
		onRightButtonPress: () => {}
	};

	render() {
		let { onRightButtonPress, name } = this.props;
		return (
			<ListItem style={{ width: '100%' }}>
				<Body>
					<Text>{name}</Text>
				</Body>
				<Right style={{ width: '30%', backgroundColor: 'red' }}>
					<Button
						style={{ width: '100%', alignItems: 'center', justifyContent: 'center' }}
						onPress={onRightButtonPress}
					>
						<Icon name="md-bicycle" />
					</Button>
				</Right>
			</ListItem>
		);
	}
}
