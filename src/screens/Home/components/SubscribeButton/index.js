import React, { Component } from 'react';
import { View, Text } from 'react-native';
import NFButton from '../../../../components/NFButton';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';

const SUBSCRIBE = gql`
	mutation SubscribeNotification($token: String!) {
		subscribeNotification(token: $token)
	}
`;
export default class SubscribeButton extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		let { token, title } = this.props;
		return (
			<Mutation mutation={SUBSCRIBE}>
				{(subscribeNotification, { data, error }) => {
					return (
						<NFButton
							title={title}
							onPress={async () => {
								console.log('user pressed here', token);
								try {
									let res = await subscribeNotification({ variables: { token } });
									console.log(res);
								} catch (e) {
									console.log('error');
									let error = JSON.stringify(e);
									let parseError = JSON.parse(error);
									console.log(parseError);
								}
							}}
						/>
					);
				}}
			</Mutation>
		);
	}
}
