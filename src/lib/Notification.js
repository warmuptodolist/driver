import firebase from 'react-native-firebase';
import { NOTIFICATION } from '../config';

class CusNotification {
	constructor() {
		this.firebase = firebase;
	}

	///this function is use for create channel in android only
	createAndroidChannel = async (
		channelId,
		channelName,
		channelImportance = NOTIFICATION.CHANNEL.IMPORTANCE.MAX,
		description = ''
	) => {
		let importance;
		switch (channelImportance) {
			case NOTIFICATION.CHANNEL.IMPORTANCE.DEFAULT:
				importance = this.firebase.notifications.Android.Importance.Default;
				break;
			case NOTIFICATION.CHANNEL.IMPORTANCE.HIGH:
				importance = this.firebase.notifications.Android.Importance.High;
				break;
			case NOTIFICATION.CHANNEL.IMPORTANCE.LOW:
				importance = this.firebase.notifications.Android.Importance.Low;
				break;
			case NOTIFICATION.CHANNEL.IMPORTANCE.MAX:
				importance = this.firebase.notifications.Android.Importance.Max;
				break;
			case NOTIFICATION.CHANNEL.IMPORTANCE.MIN:
				importance = this.firebase.notifications.Android.Importance.Min;
				break;
			case NOTIFICATION.CHANNEL.IMPORTANCE.NONE:
				importance = this.firebase.notifications.Android.Importance.None;
				break;
			default:
				importance = this.firebase.notifications.Android.Importance.Unspecified;
				break;
		}
		const channel = new this.firebase.notifications.Android.Channel(
			channelId,
			channelName,
			importance
		).setDescription(description);
		console.log(channel, importance);
		return await this.firebase.notifications().android.createChannel(channel);
	};

	createAndroidChannelGroup = (channelGroupID = '', channelGroupName = '') => {
		const channelGroup = new this.firebase.notifications.Android.ChannelGroup(channelGroupID, channelGroupName);
		return this.firebase.notifications().android.createChannelGroup(channelGroup);
	};

	askPermission = async () => {
		try {
			return await this.firebase.messaging().requestPermission();
		} catch (e) {
			throw e;
		}
	};

	hasPermission = async () => {
		const enabled = await firebase.messaging().hasPermission();
		if (enabled) {
			return true;
		}
		return false;
	};

	getFCMToken = async () => {
		try {
			return await firebase.messaging().getToken();
		} catch (e) {
			throw e;
		}
	};

	onNofication = (cb = (notification) => {}) => {
		return this.firebase.notifications().onNotification(cb);
	};
}

const Notification = new CusNotification();

export default Notification;
