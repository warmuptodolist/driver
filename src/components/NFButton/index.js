import React, { Component } from 'react';
import { Text, View, Button } from 'react-native';

export default class NFButton extends Component {
	static defaultProps = {
		title: 'submit',
		onPress: () => {}
	};
	render() {
		let { title, onPress } = this.props;

		return (
			<View style={{ height: '20%', width: '50%', backgroundColor: 'green' }}>
				<Button
					title={title}
					onPress={() => {
						console.log('excuted onPress');
						onPress();
					}}
				/>
			</View>
		);
	}
}
