import React, { Component } from 'react';
import { View, Text } from 'react-native';
import SubscribeButton from './components/SubscribeButton/index';
import Notification from '../../lib/Notification';
import firebase from 'react-native-firebase';
import DeliveryItem from './components/DeliveryItem/index';
import DeliveryList from './components/DeliveryList';
import { NOTIFICATION_TOPIC } from '../../config';
export default class Home extends Component {
	constructor() {
		super();
		this.state = {
			token: '',
		};
	}
	async componentDidMount() {
		let token = await Notification.getFCMToken();
		this.setState(
			{
				token,
			},
			() => {
				console.log('this is token ======>', this.state.token);
			},
		);
		firebase.messaging().subscribeToTopic(NOTIFICATION_TOPIC.REJECTED_DELIVERY);
		firebase.messaging().subscribeToTopic(NOTIFICATION_TOPIC.ACCEPTED_DELIVERY);

		this.notificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification) => {
			console.log('on display notification');
		});
		Notification.onNofication(this.handleNotification);
		this.messageListener = firebase.messaging().onMessage((message) => {
			// Process your message as required
			console.log('firebase cloud message ====>', message);
		});
	}

	handleNotification = (notification) => {
		console.log('this is notification ====>', notification);
		let notificationTitle = notification._title;
		let notificationBody = notification._body;
		let notificationData = notification._data;

		///create Channel for android notificaion
		const channel = new firebase.notifications.Android.Channel(
			'joonaak_channel',
			'Joonaak_Channel',
			firebase.notifications.Android.Importance.Max,
		).setDescription('My apps test channel');

		// Create the channel
		firebase.notifications().android.createChannel(channel);
		const myNotification = new firebase.notifications.Notification()
			.setNotificationId('notificationId')
			.setTitle(notificationTitle)
			.setBody(notificationBody)
			.setData({
				...notificationData,
			});

		myNotification.android.setChannelId('joonaak_channel').android.setSmallIcon('ic_launcher');

		firebase.notifications().displayNotification(myNotification);
	};
	render() {
		let { token } = this.state;
		return (
			<View style={{ justifyContent: 'center', alignItems: 'center' }}>
				{/* <SubscribeButton token={token} title="Start Delivery" /> */}
				<Text>Customer List</Text>
				<DeliveryList />
			</View>
		);
	}
}
